<?php 
 class Beranda_admin extends CI_Controller{
 	public function index(){
  			$this->load->model('record_model');
 			$data['konsumen'] =  $this->record_model->konsumen();
 			$data['barang'] =  $this->record_model->barang();
 			$data['penjualan'] =  $this->record_model->penjualan();
  		$this->load->view('templates_admin/header',$data);
 		$this->load->view('templates_admin/sidebar',$data);
 		$this->load->view('admin/Beranda_admin',$data);
 }
}

 ?>