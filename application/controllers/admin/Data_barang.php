<?php 
class Data_barang extends CI_Controller{ 
public function index(){
	$this->load->model('model_barang');
	 	$data['barang']=$this->model_barang->tampil_data()->result();
	    $this->load->view('templates_admin/header');
 		$this->load->view('templates_admin/sidebar');
		 $this->load->view('templates_admin/footer');
		 $this->load->view('admin/data_barang', $data);
}
public function tambah_aksi(){
	$this->load->model('model_barang');
	$nama_barang	 = $this->input->post('nama_barang');
	$harga			 = $this->input->post('harga');
	$stok			 = $this->input->post('stok');
	$kategori		 = $this->input->post('kategori');

	$data = array (
		'nama_barang' => $nama_barang,
		'harga' =>$harga,
		'stok' => $stok,
		'kategori' => $kategori
	);

	$this->model_barang->tambah_barang($data, 'tb_barang');
	$this->session->set_flashdata('message','<div class="alert alert-warning alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah ditambahkan</div>');
	redirect('admin/data_barang/index');
}

public function edit($id){
	$this->load->model('model_barang');
	$where = array('id' => $id);
	$data['barang'] = $this->model_barang->edit_barang($where, 'tb_barang')->result();
	$this->load->view('templates_admin/header');
	 $this->load->view('templates_admin/sidebar');
	 $this->load->view('templates_admin/footer');
	 $this->load->view('admin/edit_barang', $data);
}
public function update(){
	$this->load->model('model_barang');
	$id				=	$this->input->post('id');
	$nama_barang	=	$this->input->post('nama_barang');
	$harga			=	$this->input->post('harga');
	$stok 	 		=	$this->input->post('stok');
	$kategori 		=	$this->input->post('kategori');

$data = array(
	'nama_barang' => $nama_barang,
	'harga' =>$harga,
	'stok' => $stok,
	'kategori' => $kategori
	);
$where= array(
	'id' =>$id
);

$this->model_barang->update_data($where,$data,'tb_barang');
$this->session->set_flashdata('message','<div class="alert alert-primary alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah diUpdate</div>');
redirect ('admin/data_barang/index');
}

public function hapus($id){
	$where = array ('id' => $id);
	$this->model_barang->hapus_data($where, 'tb_barang');
	$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah dihapus</div>');
	redirect('admin/data_barang/index');
}
public function detail($id){
	$this->load->model('model_barang');
	 $data['barang'] = $this->model_barang->detail_barang($id);
	 $this->load->view('templates_admin/header');
	 $this->load->view('templates_admin/sidebar');
	$this->load->view('templates_admin/footer');
	 $this->load->view('admin/detail_barang', $data);
 }


public function pdf(){
 		$this->load->model('model_barang');
 		$this->load->library('dompdf_gen');

 		$data['barang'] = $this->model_barang->tampil_data()->result();
 		$this->load->view('export_barang', $data);

 		$paper_size = 'A4';
 		$orientation = 'landscape';
 		$html = $this->output->get_output();
 		$this->dompdf->set_paper($paper_size, $orientation);

 		$this->dompdf->load_html($html);
 		$this->dompdf->render();
 		$this->dompdf->stream("baca_data.pdf", array('Attachment' =>0));
 	} 
public function search(){
 		$this->load->model('model_barang');
 		$keyword = $this->input->post('keyword');
 		$data['barang']=$this->model_barang->get_keyword($keyword);
 		$this->load->view('templates_admin/header');
 		$this->load->view('templates_admin/sidebar');
 		$this->load->view('admin/data_barang', $data);
 		$this->load->view('templates_admin/footer');
 	}
}
?>