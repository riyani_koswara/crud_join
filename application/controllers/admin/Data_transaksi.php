<?php 
class Data_transaksi extends CI_Controller{ 
	 	public function index(){
		$pembeli = 1;
	 	$this->load->model('model_transaksi');
		$data['transaksi'] = $this->model_transaksi->tampil_data($pembeli);
	    $this->load->view('templates_admin/header');
 		$this->load->view('templates_admin/sidebar');
 		$this->load->view('admin/data_transaksi', $data);
 		$this->load->view('templates_admin/footer');
}

public function pdf(){
 		$this->load->model('model_transaksi');
 		$this->load->library('dompdf_gen');

 		$data['transaksi'] = $this->model_transaksi->tampil_data()->result();
 		$this->load->view('bacaan_pdf', $data);

 		$paper_size = 'A4';
 		$orientation = 'landscape';
 		$html = $this->output->get_output();
 		$this->dompdf->set_paper($paper_size, $orientation);

 		$this->dompdf->load_html($html);
 		$this->dompdf->render();
 		$this->dompdf->stream("baca_materi.pdf", array('Attachment' =>0));
 	} 

}
?>