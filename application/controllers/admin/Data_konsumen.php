<?php 
class Data_konsumen extends CI_Controller{ 
public function index(){
	$this->load->model('model_pembeli');
	$data['konsumen']=$this->model_pembeli->tampil_data()->result();
	    $this->load->view('templates_admin/header');
 		$this->load->view('templates_admin/sidebar');
 		$this->load->view('admin/data_konsumen', $data);
 		$this->load->view('templates_admin/footer');
}

public function tambah_aksi(){
	$this->load->model('model_pembeli');
	$nama_pembeli	 = $this->input->post('nama_pembeli');
	$alamat			 = $this->input->post('alamat');
	$email			 = $this->input->post('email');

	$data = array (
		'nama_pembeli' => $nama_pembeli,
		'alamat' =>$alamat,
		'email' => $email
	);

	$this->model_pembeli->tambah_pembeli($data, 'tb_pembeli');
	$this->session->set_flashdata('message','<div class="alert alert-warning alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah ditambahkan</div>');
	redirect('admin/data_konsumen/index');
}

public function edit($id){
	$this->load->model('model_pembeli');
	$where = array('id' => $id);
	$data['konsumen'] = $this->model_pembeli->edit_pembeli($where, 'tb_pembeli')->result();
	$this->load->view('templates_admin/header');
	 $this->load->view('templates_admin/sidebar');
	 $this->load->view('templates_admin/footer');
	 $this->load->view('admin/edit_pembeli', $data);
}
public function update(){
	$this->load->model('model_pembeli');
	$id				=	$this->input->post('id');
	$nama_pembeli	=	$this->input->post('nama_pembeli');
	$alamat			=	$this->input->post('alamat');
	$email 	 		=	$this->input->post('email');

$data = array(
	'nama_pembeli' => $nama_pembeli,
	'alamat' =>$alamat,
	'email' => $email
	);
$where= array(
	'id' =>$id
);

$this->model_pembeli->update_data($where,$data,'tb_pembeli');
$this->session->set_flashdata('message','<div class="alert alert-primary alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah diUpdate</div>');
redirect ('admin/data_konsumen/index');
}

public function hapus($id){
	$where = array ('id' => $id);
	$this->model_pembeli->hapus_data($where, 'tb_pembeli');
	$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah dihapus</div>');
	redirect('admin/data_konsumen/index');
}
public function detail($id){
	$this->load->model('model_pembeli');
	 $data['konsumen'] = $this->model_pembeli->detail_pembeli($id);
	 $this->load->view('templates_admin/header');
	 $this->load->view('templates_admin/sidebar');
	$this->load->view('templates_admin/footer');
	 $this->load->view('admin/detail_pembeli', $data);
 }


public function pdf(){
 		$this->load->model('model_pembeli');
 		$this->load->library('Dompdf_gen');

 		$data['konsumen'] = $this->model_pembeli->tampil_data()->result();
 		$this->load->view('export_konsumen', $data);

 		$paper_size = 'A4';
 		$orientation = 'landscape';
 		$html = $this->output->get_output();
 		$this->dompdf->set_paper($paper_size, $orientation);

 		$this->dompdf->load_html($html);
 		$this->dompdf->render();
 		$this->dompdf->stream("baca_data.pdf", array('Attachment' =>0));
	 } 
	 
	 public function search(){
		$this->load->model('model_pembeli');
		$keyword = $this->input->post('keyword');
		$data['konsumen']=$this->model_pembeli->get_keyword($keyword);
		$this->load->view('templates_admin/header');
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/data_konsumen', $data);
		$this->load->view('templates_admin/footer');
	}

}
?>