<?php 
class Model_transaksi extends CI_Model{
	public function tampil_data($id_pembeli){
		if (!empty($id_pembeli)){
			$this->db->where('tb_pembeli.id', $id_pembeli);
		}

		$this->db->select(array(
			'tb_pembeli.*',
			'tb_transaksi.tanggal as tanggal_transaksi',
			'tb_transaksi.total as total_transaksi',
			'tb_barang.nama_barang',
			'tb_barang.harga',
		));
		$this->db->from('tb_pembeli');
		$this->db->join('tb_transaksi','tb_transaksi.id_pembeli=tb_pembeli.id');
		$this->db->join('tb_barang','tb_barang.id=tb_transaksi.id_pembeli');
		$data = $this->db->get();
		return $data->result();
	}
}

?>