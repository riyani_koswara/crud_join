<body>
    <div class="wrapper">
        <nav id="sidebar">
            <div class="sidebar-header">
            <hr color="white" />
            <ul class="list-unstyled components">
                <div class="foto">
                <img src="<?php echo base_url('template/img/sikos.jpg') ?>" height = "150px" width="150px" class="img-circle">
            </div>
            <hr color="white" />

                <li class="active">
                    <a href="<?php echo base_url('beranda_admin/index')?>">
                        <i class="fas fa-home"></i>
                        Dashboard
                    </a>
                    </li>
                <li>
                    <a href="<?php echo base_url('admin/data_konsumen')?>">
                    <i class="fas fa-users"></i>
                        Data Konsumen
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/data_barang')?>">
                    <i class="fas fa-box"></i>
                        Data Barang
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/data_penjualan') ?>">
                    <i class="fas fa-dollar-sign"></i>
                        Data Penjualan
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/data_transaksi') ?>">
                    <i class="fas fa-hand-holding-usd"></i>
                        Data Transaksi
                    </a>
                </li>
                    </ul>

        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-white bg-white" style="height: 100px;">
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-5 my-2 my-md-0 mw-100 search 
          navbar-search">
            <div class="input-group">
                <div class="input-group-append">
                  </div>

            </div>
        </form>

        </nav>
    <!-- jQuery CDN-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>

</html>