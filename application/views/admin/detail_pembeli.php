<div class="card mb-3">
  <div class="card-header text-center">   
   <strong>Detail Data Konsumen</strong>
   <div class="class" style="float: right;">
          <?php echo anchor('admin/data_konsumen/index/','<div class="btn btn-warning"><i class="fas fa-hand-point-left"></i></div>') ?>
     </div>
   </div>
  <div class="card-body">
    <div class="row text-center">
    <?php foreach ($konsumen as $ksm) : ?>
    		<div class="col-md-12">
    			<table class="table">
    				<tr>
    					<h6>Nama Pembeli : <?php echo $ksm->nama_pembeli ?></h6>
                        <h6>Alamat : <?php echo $ksm->alamat ?></h6>
                        <h6>Email : <?php echo $ksm->email ?></h6>
    				</tr>
    			</table>
    		</div>
    	</div>

    <?php endforeach; ?>
    </div>
      </div>
</div>