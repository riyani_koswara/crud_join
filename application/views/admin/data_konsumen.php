<?php echo $this->session->flashdata('message') ?>
     <div class="row">
      <div class="col-md-12">
        <div class="col-md-12">
         <div class="card card-primary">
          <div class="card-header">
          <h3 class="card-title">Data Konsumen</h3>
           <div class="card-tools">
            <a class="btn btn-warning btn-sm" style="margin-top: -40px;  margin-left: -600px;" href="<?php echo base_url('admin/data_konsumen/pdf') ?>">
      <i class="fas fa-file"></i> Export PDF</a>
            <div class="input-group-append">
  <button class="btn btn-sm btn-default okok" data-toggle='modal'
   data-target='#tambah_konsumen'>
    <i class="fas fa-plus fa-sm"></i>Tambah Data</button>
    <div class="d-none d-sm-inline-block form-inline search" style="margin-top: -45px; margin:left:300px;">
          <?php echo form_open('admin/data_konsumen/search'); ?>
            <div class="input-group">
                <div class="input-group-append">
                         </div>
              <input type="text" class="form-control border-3 small" name="keyword" placeholder="Search for...">
               <button class="btn btn-primary" type="button">
                <i class="fas fa-search fa-sm"></i>
                </button>
                <?php   echo form_close() ?>
            </div>
        </div>
    </div>
  </div>
</div>

	<div class="row">
    <div class="col-lg-12">
       <table class="table table-bordered table-hover table-striped table-center mt-3">
        <thead class="thead-light">
		<tr>
			<th scope="col">Nomor</th>
			<th scope="col">Nama Konsumen</th>
			<th scope="col">Alamat</th>
      <th scope="col">Email</th>
      <th colspan="3">Aksi</th>
		</tr>
		<?php 
			foreach ($konsumen as $ksm) : ?>
				<tr>
					<td><?php echo $ksm->id ?></td>
					<td><?php echo $ksm->nama_pembeli ?></td>
					<td><?php echo $ksm->alamat ?></td>
          <td><?php echo $ksm->email ?></td>
          <td><?php echo anchor ('admin/data_konsumen/detail/' .$ksm->id,
           '<div class="btn btn-success btn-sm"><i class="fas fa-search-plus"></i></div>') ?></td>
          <td><?php echo anchor ('admin/data_konsumen/edit/' .$ksm->id,
           '<div class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></div>') ?></td>
          <td><?php echo anchor ('admin/data_konsumen/hapus/' .$ksm->id, 
          '<div class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></div>') ?></td>
				</tr>
			<?php endforeach; ?>
        </thead>
      </table>
    </div>
  </div>
<!-- Modal -->
<div class="modal fade" id="tambah_konsumen" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">INPUT Data Konsumen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(). 'admin/data_konsumen/tambah_aksi'; ?>" 
        	method="post" enctype="multipart/form-data">

        	<div class="form-group">
        		<label>Nama Konsumen</label>
        		<input type="text" name="nama_pembeli" class="form-control">
        	</div>

          <div class="form-group">
        		<label>Alamat</label>
          <input type="text" name="alamat" class="form-control">
     </div>

         <div class="form-group">
        		<label>Email</label>
        		<input type="text" name="email" class="form-control">
          </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  </form>
    </div>
  </div>
</div>
