<div class="content">
    <div class="container-fluid">
     <div class="row">
      <div class="col-md-12">
        <div class="col-md-12">
         <div class="card card-primary">
          <div class="card-header">
          <h3 class="card-title"><i class="fas fa-edit"></i>Edit Data Pembeli</h3>
      </div>

	<?php foreach ($konsumen as $ksm): ?>
		<form method="post" action="<?php echo base_url().'admin/data_konsumen/update' ?>">
			<div class="for-group mt-3">
				<label>Nama Konsumen</label>
				<input type="text" name="nama_pembeli" class="form-control" value="<?php echo $ksm->nama_pembeli ?>">
			</div>

			<div class="for-group">
				<label>Alamat</label>
				<input type="hidden" name="id" class="form-control" value="<?php echo $ksm->id ?>">
				<input type="text" name="alamat" class="form-control" value="<?php echo $ksm->alamat ?>">
			</div>

			<div class="for-group">
				<label>Email</label>
				<input type="hidden" name="id" class="form-control" value="<?php echo $ksm->id ?>">
				<input type="text" name="email" class="form-control" value="<?php echo $ksm->email ?>">
			</div>
        </div>
		
		<button type="submit" class="btn btn-primary btn-sm mt-3">Update</button>
	</form>

	<?php endforeach; ?>
</div>
</div>
</div>
</div>
</div>
</div>