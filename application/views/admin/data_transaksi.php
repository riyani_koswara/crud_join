<div class="row">
      <div class="col-md-12">
        <div class="col-md-12">
         <div class="card card-primary">
          <div class="card-header">
          <h3 class="card-title">Data Transaksi</h3>
           <div class="card-tools">
            <a class="btn btn-warning btn-sm" style="margin-top: -30px;" href="<?php echo base_url('admin/data_transaksi/pdf') ?>">
      <i class="fas fa-file"></i> Export PDF</a>
  </div>
</div>

<section class="content">  
<div class="box">    
  <div class="box-header">
  </div>
  <div class="box-body table-responsive">
     <table class="table table-bordered table-hover table-striped table-center mt-3">
        <thead class="thead-light">
    <tr>
      <th scope="col">Nomor</th>
      <th scope="col">Nama Konsumen</th>
      <th scope="col">Nama Barang</th>
      <th scope="col">Harga</th>
      <th scope="col">Total Jual</th>
      <th scope="col">Tanggal Transaksi</th>

    </tr>
    <?php $no=1;
     foreach ($transaksi as $key => $tks) {  ?>
      <tr>
        <td><?=$no++?></td>
        <td><?=$tks->nama_pembeli; ?></td>
        <td><?=$tks->nama_barang?></td>
        <td><?=$tks->harga; ?></td>
        <td><?=$tks->total_transaksi?></td>
        <td><?=$tks->tanggal_transaksi?></td>
      </tr>
      <?php
    } ?>
</thead>
</table>
  </div>
</div>
</section>
