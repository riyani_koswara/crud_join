<?php echo $this->session->flashdata('message') ?>
    <div class="row">
        <div class="col-md-12">
          <div class="col-md-12">
         <div class="card card-primary">
          <div class="card-header">
          <h3 class="card-title">Data Barang</h3>
           <div class="card-tools">
           <a class="btn btn-warning btn-sm" style="margin-top: -35px; margin-left: -650px;" href="<?php echo base_url('admin/data_barang/pdf') ?>">
      <i class="fas fa-file"></i> Export PDF</a>
            <div class="input-group-append">
  <button class="btn btn-sm btn-default okok" data-toggle='modal'
   data-target='#tambah_barang'>
    <i class="fas fa-plus fa-sm"></i>Tambah Data</button>
    <div class="d-none d-sm-inline-block form-inline search" style="margin-top: -45px; margin:left:300px;">
          <?php echo form_open('admin/data_barang/search'); ?>
            <div class="input-group">
                <div class="input-group-append">
                         </div>
              <input type="text" class="form-control border-3 small" name="keyword" placeholder="Search for...">
               <button class="btn btn-primary" type="button">
                <i class="fas fa-search fa-sm"></i>
                </button>
                <?php   echo form_close() ?>
            </div>
        </div>
    </div>
  </div>
</div>

	<div class="row">
    <div class="col-lg-12">
       <table class="table table-bordered table-hover table-striped table-center mt-3">
        <thead class="thead-light">
		<tr>
			<th scope="col">Nomor</th>
			<th scope="col">Nama Barang</th>
			<th scope="col">Harga</th>
      <th scope="col">Stok</th>
			<th scope="col">Kategori</th>
      <th colspan="3">Aksi</th>
		</tr>
		<?php 
			foreach ($barang as $brg) : ?>
				<tr>
					<td><?php echo $brg->id ?></td>
					<td><?php echo $brg->nama_barang ?></td>
					<td><?php echo $brg->harga ?></td>
          <td><?php echo $brg->stok ?></td>
					<td><?php echo $brg->kategori ?></td>
          <td><?php echo anchor ('admin/data_barang/detail/' .$brg->id,
           '<div class="btn btn-success btn-sm"><i class="fas fa-search-plus"></i></div>') ?></td>
          <td><?php echo anchor ('admin/data_barang/edit/' .$brg->id,
           '<div class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></div>') ?></td>
          <td><?php echo anchor ('admin/data_barang/hapus/' .$brg->id, 
          '<div class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></div>') ?></td>
				</tr>
			<?php endforeach; ?>
        </thead>
      </table>
    </div>
  </div>
<!-- Modal -->
<div class="modal fade" id="tambah_barang" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">INPUT Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(). 'admin/data_barang/tambah_aksi'; ?>" 
        	method="post" enctype="multipart/form-data">

        	<div class="form-group">
        		<label>Nama Barang</label>
        		<input type="text" name="nama_barang" class="form-control">
        	</div>

          <div class="form-group">
        		<label>Harga</label>
          <input type="text" name="harga" class="form-control">
     </div>

         <div class="form-group">
        		<label>Stok</label>
        		<input type="text" name="stok" class="form-control">
        	</div>

          <div class="form-group">
            <label>Kategori</label>
            <select class="form-control" name="kategori">
              <option>Makanan</option>
              <option>Gaya Hidup</option>
              <option>Pakaian Wanita</option>
              <option>Pakaian Pria</option>
            </select>         
          </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  </form>
    </div>
  </div>
</div>
